﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mobScript : MonoBehaviour
{
    [Header("Health")]
    public float maxHealth = 10f;
    public float health = 10f;
    [Header("GameObjects")]
    public GameObject healthBar;



    public void DecreaseHealth(float minDmg, float maxDmg, float critChance) {
        float damage = Random.Range(minDmg, maxDmg);
        if(Random.Range(0,100)<critChance) {
            damage=damage*2;
        }    
        health=health-damage;
        if(health<=0) {
            Destroy(this.gameObject);
        }
        healthBar.transform.localScale = new Vector2((1f/(maxHealth/health)) / 2, 0.5f);   
    }
}
