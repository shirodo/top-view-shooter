﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CharacterMovement : MonoBehaviour {
    public float hareketHizi = 5;
    [Header("Character Stamina")]
    public float maxStamina = 100;
    public float currentStamina;
    [Header("Character Speed")]
    [Range(1, 3)]
    public float WalkSpeed;
    [Range(4, 8)]
    public float RunningSpeed;
    [Header("StaminaBar, Healthbar etc.")]
    public Rigidbody2D rigidBody;
    public Slider staminaBar;
    [Header("Animations")]
    public Animator animator;
    Vector2 hareket;
    bool characterMoving = false;
    void Start() {
        hareketHizi=WalkSpeed;
    }
    void Update() {
        if(Input.GetKey(KeyCode.W)||Input.GetKey(KeyCode.A)||Input.GetKey(KeyCode.S)||Input.GetKey(KeyCode.D)) {
            animator.SetBool("isWalking", true);
        }
        else {
            animator.SetBool("isWalking", false);
        }
        if(currentStamina<0f) {
            hareketHizi=WalkSpeed;
        }
        staminaBar.value=currentStamina;
        if(Input.GetKeyDown(KeyCode.LeftShift)) {
            if(currentStamina>1f) {
                hareketHizi=RunningSpeed;
            }
            characterMoving=true;
        }
        else if(Input.GetKeyUp(KeyCode.LeftShift)) {
            hareketHizi=WalkSpeed;
            characterMoving=false;
        }
        if(!characterMoving&&currentStamina<=100) {
            currentStamina+=15*Time.deltaTime;
        }
        else if(characterMoving&&currentStamina>0) {
            currentStamina-=24*Time.deltaTime;
        }
        if(Input.GetKeyDown(KeyCode.D)||Input.GetKeyDown(KeyCode.RightArrow)) {
            this.GetComponent<SpriteRenderer>().flipX=false;
        }
        else if(Input.GetKeyDown(KeyCode.A)||Input.GetKeyDown(KeyCode.LeftArrow)) {
            this.GetComponent<SpriteRenderer>().flipX=true;
        }
        hareket.x=Input.GetAxisRaw("Horizontal");
        hareket.y=Input.GetAxisRaw("Vertical");
    }

    void FixedUpdate() {
        rigidBody.MovePosition(rigidBody.position+hareket*hareketHizi*Time.fixedDeltaTime);
    }

}
