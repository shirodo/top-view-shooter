﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookTowardMouse : MonoBehaviour {
    public int rotation;
    [Range(-360, 360)]
    public float degree;
    bool right = false;
    void Update() {
        if(Input.GetKeyDown(KeyCode.D)||Input.GetKeyDown(KeyCode.RightArrow)) {
            if(right) {
                this.transform.position=new Vector2(this.gameObject.transform.position.x+(-0.12f*4), this.gameObject.transform.position.y);
                this.gameObject.GetComponent<SpriteRenderer>().flipX=false;
                right=false;
            }
        }
        else if(Input.GetKeyDown(KeyCode.A)||Input.GetKeyDown(KeyCode.LeftArrow)) {
            if(!right) {
                this.transform.position=new Vector2(this.gameObject.transform.position.x+(0.12f*4), this.gameObject.transform.position.y);
                right=true;
                this.gameObject.GetComponent<SpriteRenderer>().flipX=true;

            }
        }

        Vector3 dir = Input.mousePosition-Camera.main.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(dir.y, dir.x)*Mathf.Rad2Deg;

        transform.rotation=Quaternion.AngleAxis(angle+90, Vector3.forward);

    }
}

