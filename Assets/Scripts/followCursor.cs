﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class followCursor : MonoBehaviour {
    public Vector3 MousePos;
    void Start() {
        Cursor.visible=false;
    }
    void Update() {
        MousePos=Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.transform.position=new Vector3(MousePos.x, MousePos.y, 0);
    }
}