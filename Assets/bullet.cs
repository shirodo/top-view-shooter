﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour {
    public float maxDamage;
    public float minDamage;
    public float criticalChance;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start() {
        Vector3 dir = Input.mousePosition-Camera.main.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(dir.y, dir.x)*Mathf.Rad2Deg;
        transform.rotation=Quaternion.AngleAxis(angle+90, Vector3.forward);
        rb.AddForce(this.transform.up*-0.06f);
        StartCoroutine(destroyMe());
    }
    IEnumerator destroyMe() {
        yield return new WaitForSeconds(0.6f);
        Destroy(this.gameObject);
    }
    private void OnTriggerStay2D(Collider2D collision) {
        if(collision.gameObject.name=="Colliders") { }
        else {
            collision.gameObject.GetComponent<mobScript>().DecreaseHealth(minDamage, maxDamage, criticalChance);
            Destroy(this.gameObject);
        }
        Destroy(this.gameObject);
    }
}
