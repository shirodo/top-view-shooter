﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour {
    public float maxSpeed;
    public Rigidbody2D bullet;
    bool shootDelay;
    void Update() {
        if(Input.GetMouseButtonDown(0) && !shootDelay) {
            StartCoroutine(shootNow());
            shootDelay=true;
        }
    }
    IEnumerator shootNow() {
        int three = 3;
        for(int i= 2; i>=0; i--) {
            Rigidbody2D bulletInstance = Instantiate(bullet, transform.position, Quaternion.Euler(new Vector3(this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z))) as Rigidbody2D;
            yield return new WaitForSeconds(0.2f);
        }
        shootDelay=false;
    }
}
